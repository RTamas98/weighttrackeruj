import {IWeightInfo, WeightInfo} from "./WeightInfo";
import {CacheService} from "./LocalStorageService";
import {buildSolutionReferences} from "ts-loader/dist/instances";

export class Weight {

    private mainElement: HTMLElement;
    public static readonly weightInfos: IWeightInfo[] = CacheService.getWrightInfos() || [];
    private history = document.querySelector('.history');
    private weight: HTMLInputElement = document.querySelector('#weight');
    private datepicker: HTMLInputElement = document.querySelector('#datepicker');
    private showData: HTMLParagraphElement;
    private myStorage;

    constructor() {
        this.mainElement = document.getElementsByTagName('main')[0];
        this.createView();
        this.createHistory();
        this.CreateTableFromJSON();
        this.myStorage = window.localStorage;
        this.maxDate();
    }


    createView() {
        const Add = document.querySelector('.form')
        Add.addEventListener("submit",this.getValues.bind(this));

    }

    createHistory() {

        this.showData = document.createElement('p');
        this.showData.id = 'showData';
        this.showData.className = 'showData__class'
        this.history.appendChild(this.showData);

    }


    getValues() {
        let currentWeight;
        currentWeight = this.weight.value;
        this.showData.innerText = currentWeight + " kg";
        let weightInfo = {
            weight: parseFloat(this.weight.value),
            date: new Date(this.datepicker.value).toLocaleDateString("hu-HU")
        }
        if (weightInfo.date) {
        }
        let today = new Date().toLocaleDateString("hu-HU");
        let yesterday = new Date().getDate() - 1;
        let thisYear = new Date().getFullYear();
        let month = new Date(this.datepicker.value).getMonth();
        let mm:string;
        switch (month){
            case 0:
                mm = 'Jan';
                break;
            case 1:
                mm = 'Feb';
                break;
            case 2:
                mm = 'Mar';
                break;
            case 3:
                mm = 'Apr';
                break;
            case 4:
                mm = 'May';
                break;
            case 5:
                mm = 'Jun';
                break;
            case 6:
                mm = 'Jul';
                break;
            case 7:
                mm = 'Aug';
                break;
            case 8:
                mm = 'Sep';
                break;
            case 9:
                mm = 'Oct';
                break;
            case 10:
                mm = 'Nov';
                break;
            case 11:
                mm = 'Dec';
                break;
        }

        if (weightInfo.date === today) {
            weightInfo.date = "Today at " + new Date().getHours() + " : " + new Date().getMinutes();
        }
        if ((new Date(this.datepicker.value).getDate()) === yesterday) {
            weightInfo.date = "Yesterday at " + new Date().getHours() + " : " + new Date().getMinutes();
        }
        if(new Date(this.datepicker.value).getDate() < yesterday && new Date(this.datepicker.value).getFullYear() == thisYear){
            weightInfo.date = (new Date(this.datepicker.value).getDate()) + ' ' + mm + ' at ' + new Date().getHours() + " : " + new Date().getMinutes();
        }
        if(new Date(this.datepicker.value).getFullYear() < thisYear){
            weightInfo.date = (new Date(this.datepicker.value).getDate()) + ' ' + mm + ' ' + new Date(this.datepicker.value).getFullYear() + ' at ' + new Date().getHours() + " : " + new Date().getMinutes();
        }


        CacheService.saveWeightInfo(weightInfo);
        Weight.weightInfos.push(weightInfo);
        this.CreateTableFromJSON();

    }

    CreateTableFromJSON() {
        const displayedWeightInfos = CacheService.getWrightInfos()?.slice(-10);
        let col = [];

        for (let i = 0; i < displayedWeightInfos.length; i++) {
            for (let key in displayedWeightInfos[i]) {
                if (col.indexOf(key) === -1) {
                    col.push(key);
                }
            }
        }

        let table = document.createElement("table");
        table.className = "table";


        let tr = table.insertRow(-1);
        for (let i = 0; i < col.length; i++) {
            let th = document.createElement("th");
            th.innerHTML = col[i];
            th.className = 'weight__history';

            tr.appendChild(th);
        }

        for (let i = displayedWeightInfos.length - 1; i >= 0; i--) {

            tr = table.insertRow(-1);
            for (let j = 0; j < col.length; j++) {
                let tabCell = tr.insertCell(-1);
                if (col[j] == "weight") {
                    tabCell.innerHTML = displayedWeightInfos[i][col[j]] + " kg";
                } else {
                    tabCell.innerHTML = displayedWeightInfos[i][col[j]];
                }
            }
        }

        let divContainer = document.getElementById("showData");
        divContainer.innerHTML = "";
        divContainer.appendChild(table);
    }

    

    maxDate(){
        let today:any = new Date();
        let dd = today.getDate();
        let mm = today.getMonth()+1;
        let yyyy = today.getFullYear();
        if(dd<10){
            dd = '0'+dd;
        }if(mm<10){
            mm='0'+mm;
        }
        today = yyyy +'-'+mm+'-'+dd;
        document.getElementById('datepicker').setAttribute('max',today);
    }

}
