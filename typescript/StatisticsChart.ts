import {Chart,
    ArcElement,
    LineElement,
    BarElement,
    PointElement,
    BarController,
    BubbleController,
    ChartConfiguration,
    DoughnutController,
    LineController,
    PieController,
    PolarAreaController,
    RadarController,
    ScatterController,
    CategoryScale,
    LinearScale,
    LogarithmicScale,
    RadialLinearScale,
    TimeScale,
    TimeSeriesScale,
    Filler,
    Legend,
    Title,
    Tooltip} from 'chart.js';

Chart.register(
    ArcElement,
    LineElement,
    BarElement,
    PointElement,
    BarController,
    BubbleController,
    DoughnutController,
    LineController,
    PieController,
    PolarAreaController,
    RadarController,
    ScatterController,
    CategoryScale,
    LinearScale,
    LogarithmicScale,
    RadialLinearScale,
    TimeScale,
    TimeSeriesScale,
    Filler,
    Legend,
    Title,
    Tooltip
);



export class StatisticsChart {

    private mainElement: HTMLElement;
    private canvas: HTMLCanvasElement;
    private chartObject: Chart;

    constructor() {
        this.mainElement = document.getElementsByTagName('section')[2];
        this.creatCanvas();
    }

    creatCanvas(){
        this.canvas = document.createElement('canvas');
        this.canvas.style.width = '100%';
        this.canvas.style.height = '500px';
        this.mainElement.appendChild(this.canvas);
    }

    public updateChart(labels:Array<string>, data:Array<number>){
        const config: ChartConfiguration = {
            type: 'line',
            data: {
                labels : labels,
                datasets: [
                    {
                        label: 'Weight tracker graphicon',
                        backgroundColor: 'rgb(139,37,40)',
                        borderColor: 'rgb(210,6,49)',
                        data: data,
                        borderWidth: 1,
                }]},
             options: {responsive:false}
        };
        const canv: CanvasRenderingContext2D = this.canvas.getContext('2d');
        if (this.chartObject == null) {
            this.chartObject = new Chart(canv,config);
        }else {
            this.chartObject.destroy();
            this.chartObject = new Chart(canv,config);
        }
    }

}
