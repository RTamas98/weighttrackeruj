import {Weight} from "./Weight";
import {StatisticsChart} from "./StatisticsChart";

export class Statistics {

    private mainElement: HTMLElement;
    private statisticsChart: StatisticsChart;

    private week = document.querySelector('.chart__switch--week');
    private month = document.querySelector('.chart__switch--month');
    private year = document.querySelector('.chart__switch--year');
    private lifeTime = document.querySelector('.chart__switch--lifetime');


    constructor() {
        this.mainElement = document.getElementsByTagName('main')[0];
        this.statisticsChart = new StatisticsChart();
        this.createCurrentWeight();
        this.createStartWeight();
        this.createProgress();
        this.createGraphiconWeek();
        this.switchButtons();
    }

    switchButtons() {
        this.week.addEventListener("mouseup", this.createGraphiconWeek.bind(this));
        this.month.addEventListener("mouseup", this.createGraphiconMonth.bind(this));
        this.year.addEventListener("mouseup", this.createGraphiconYear.bind(this));
        this.lifeTime.addEventListener("mouseup", this.createGraphiconLifetime.bind(this));
    }

    createGraphiconWeek() {

        let labels: Array<string> = [];
        let data: Array<number> = [];
        for (let i = 0; i <= 7; i++) {
            if (Weight.weightInfos[i] == null) {
                break;
            }
            labels.push(Weight.weightInfos[i]["date"]);
            data.push(Weight.weightInfos[i]["weight"]);

        }

        labels.sort();
        this.statisticsChart.updateChart(labels, data);
    }

    createGraphiconMonth() {
        let labels: Array<string> = [];
        let data: Array<number> = [];
        let j;

        for (let i = 0; i <= 30; i++) {
            if (Weight.weightInfos[i] == null) {
                break;
            }
            if (Weight.weightInfos[i]["date"] == j) {
                labels.push(" ");
            } else {
                labels.push(Weight.weightInfos[i]["date"]);
                j = Weight.weightInfos[i]["date"];
            }
            data.push(Weight.weightInfos[i]["weight"]);
        }
        labels.sort();
        this.statisticsChart.updateChart(labels, data);
    }

    createGraphiconYear() {
        let labels: Array<string> = [];
        let data: Array<number> = [];
        for (let i = 0; i <= 365; i++) {
            if (Weight.weightInfos[i] == null) {
                break;
            }
            labels.push(Weight.weightInfos[i]["date"]);
            data.push(Weight.weightInfos[i]["weight"]);
        }
        labels.sort();
        this.statisticsChart.updateChart(labels, data);
    }

    createGraphiconLifetime() {
        let labels: Array<string> = [];
        let data: Array<number> = [];
        for (let i = 0; i <= Weight.weightInfos.length - 1; i++) {
            if (Weight.weightInfos[i] == null) {
                break;
            }
            labels.push(Weight.weightInfos[i]["date"]);
            data.push(Weight.weightInfos[i]["weight"]);
        }
        labels.sort();
        this.statisticsChart.updateChart(labels, data);
    }


    createCurrentWeight() {
        const current__weight: HTMLElement = document.querySelector('.card__weight--current');
        if (Weight.weightInfos.length > 0) {
            current__weight.innerText = Weight.weightInfos[Weight.weightInfos.length - 1].weight.toFixed(1);
        } else {
            current__weight.innerText = '';
        }
    }

    createStartWeight() {
        const start__weight: HTMLElement = document.querySelector('.card__weight--start');
        if (Weight.weightInfos.length > 0) {
            start__weight.innerText = Weight.weightInfos[0].weight.toFixed(1);
        } else {
            start__weight.innerText = ""
        }

    }


    createProgress() {
        let dif: number;
        let difference: string;

        if (Weight.weightInfos.length > 1) {
            dif = Weight.weightInfos[Weight.weightInfos.length - 1].weight - Weight.weightInfos[0].weight;
        } else {
            dif = 0;
        }
        if (dif < 0) {
            difference = dif.toFixed(1) + " kg";
        } else if (dif > 0) {
            difference = "+ " + dif.toFixed(1) + " kg";
        } else {
            difference = "0 kg";
        }
        const difference__weight: HTMLElement = document.querySelector('.card__weight--progress');
        difference__weight.innerText = difference;
    }

}


