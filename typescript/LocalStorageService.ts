import {IWeightInfo} from "./WeightInfo";

export class CacheService {
    constructor() {
    }

    static saveWeightInfo(weightInfo: IWeightInfo){
        const newWeightInfos = this.getWrightInfos();
        newWeightInfos.push(weightInfo);
        
        window.localStorage.setItem('WrightInfos', JSON.stringify(newWeightInfos));
    }

    static getWrightInfos(){
        const items = window.localStorage.getItem('WrightInfos') || '[]'
        return JSON.parse(items);
    }
}