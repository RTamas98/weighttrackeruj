export class WeightInfo {

    constructor(private readonly date: Date, private readonly weight: number) {
        this.validateDate(date);
    }

    validateDate(date: Date) {
        if (date.getDay() > new Date().getDay()) {
            throw new Error("Date is greather than today");
        }
    }

    getInfos() {
        return {
            date: this.date,
            weight: this.weight
        }
    }
}

export interface IWeightInfo {
    date: string,
    weight: number
}
