const path = require('path');

module.exports = {
    mode: 'development',
    entry: './typescript/Main.ts',
    output: {
        filename: 'script.js',
        path: path.resolve(__dirname, 'dist/js')
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: ['.ts', '.js']
    },
    plugins: [
        //new cleanPlugin.CleanWebpackPlugin()
    ]
};
